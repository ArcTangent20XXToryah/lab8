﻿using UnityEngine;
using System.Collections;
using System.Runtime.Serialization.Formatters.Binary;
using System.IO;
using UnityEngine.Networking;
using UnityEngine.SceneManagement;

public class ClientConnection : MonoBehaviour
{
    int clientSocketID = -1;
    //Will store the unique identifier of the session that keeps the connection between the client
    //and the server. You use this ID as the 'target' when sending messages to the server.
    int clientServerConnectionID = -1;
    int maxConnections = 10;
    byte unreliableChannelID;
    byte reliableChannelID;
    bool isClientConnected = false;

    void Start()
    {
        DontDestroyOnLoad(this);

        //Build the global config
        GlobalConfig globalConfig = new GlobalConfig();
        //Build the channel config
        ConnectionConfig connectionConfig = new ConnectionConfig();
        //Create the host topology
        HostTopology hostTopology = new HostTopology(connectionConfig, maxConnections);
        //Initialize the network transport
        NetworkTransport.Init(globalConfig);
        //Open a socket for the client
        clientSocketID = NetworkTransport.AddHost(hostTopology, 7777);
        //Make sure the client created the socket successfully
        if (clientSocketID < 0)
        {
            Debug.Log("Client socket creation failed!");
        }
        else
            Debug.Log("Client socket creation success");
        //Create a byte to store a possible error
        byte possibleError;
        //Connect to the server using 
        //int NetworkTransport.Connect(int socketConnectingFrom, string ipAddress, int port, 0, out byte possibleError)
        //Store the ID of the connection in clientServerConnectionID
        clientServerConnectionID = NetworkTransport.Connect(clientSocketID, "localhost", 7777, 0, out possibleError);
        //Display the error (if it did error out)
        if (possibleError != (byte)NetworkError.Ok)
        {
            NetworkError networkError = (NetworkError)possibleError;
            Debug.Log("Error: " + networkError.ToString());
        }
    }

    void Update()
    {
        //If the client failed to create the socket, leave this function
        if (!isClientConnected)
        {
            return;
        }
        PollBasics();

        //If the user pressed the Space key
        //Send a message to the server "FirstConnect"
        if(Input.GetKeyDown("space"))
        {
            SendMessage("FirstConnect");
        }
        //If the user pressed the R key
        //Send a message to the server "Random message!"
        if (Input.GetKeyDown("R"))
        {
            SendMessage("Random Message!");
        }
    }

    void SendMessage(string message)
    {
        //create a byte to store a possible error
        //Create a buffer to store the message
        //Create a memory stream to send the information through
        //Create a binary formatter to serialize and translate the message into binary
        //Serialize the message
        byte error;
        byte[] buffer = new byte[1024];
        Stream memoryStream = new MemoryStream(buffer);
        BinaryFormatter binaryFormatter = new BinaryFormatter();

        binaryFormatter.Serialize(memoryStream, message);

        //Send the message from this client, over the client server connection, using the reliable channel
        NetworkTransport.Send(clientSocketID, clientServerConnectionID, reliableChannelID, buffer, (int)memoryStream.Position, out error);
        //Display the error (if it did error out)


        //NetworkTransport.Send(serverSocketID, target, reliableChannelID, buffer, (int)memoryStream.Position, out error);

        if (error != (byte)NetworkError.Ok)
        {
            NetworkError networkError = (NetworkError)error;
            Debug.Log("Error: " + networkError.ToString());
        }
    }

    void InterperateMessage(string message)
    {
        //if the message is "goto_NewScene"
        //load the level named "Scene2"
        if (message == "goto_NewScene")
            SceneManager.LoadScene("Scene2");
    }

    void PollBasics()
    {
        //prepare to receive messages by practicing good bookkeeping
        if (!isClientConnected)
        {
            return;
        }

        int recHostId;
        int connectionId;
        int channelId;
        int dataSize;
        byte[] buffer = new byte[1024];
        byte error;

        NetworkEventType networkEvent = NetworkEventType.DataEvent;
        //do
        //Receive network events
        //switch on the network event types
        //if nothing, do nothing
        //if connection
        //verify that the message was meant for me
        //debug out that i connected to the server, and display the ID of what I connected to
        //set my bool that is keeping track if I am connected to a server to true
        //if data event
        //verify that the message was meant for me and if I am connected to a server
        //decode the message (bring it through the memory stream, deseralize it, translate the binary)
        //Debug the message and the connection that the message was sent from 
        //InterperateMessage(//the message to interperate);
        //if disconnection
        //verify that the message was meant for me, and that I am disconnecting from the current connection I have with the server
        //debug that I disconnected
        //set my bool that is keeping track if I am connected to a server to false
        //while (the network event I am receiving is not Nothing)
        do
        {
            networkEvent = NetworkTransport.Receive(out recHostId, out connectionId, out channelId, buffer, 1024, out dataSize, out error);
            switch (networkEvent)
            {
                case NetworkEventType.Nothing:
                    break;
                case NetworkEventType.ConnectEvent:
                    if (recHostId == clientSocketID)
                    {
                        Debug.Log("Client: Player " + connectionId.ToString() + " connected!");
                        isClientConnected = true;
                    }
                    break;
                case NetworkEventType.DataEvent:
                    if (recHostId == clientSocketID)
                    {
                        Stream memoryStream = new MemoryStream(buffer);

                        BinaryFormatter binaryFormatter = new BinaryFormatter();

                        string message = binaryFormatter.Deserialize(memoryStream).ToString();

                        Debug.Log("Client: Received Data from " + connectionId.ToString() + "! Message: " + message);

                        InterperateMessage(message);
                    }
                    break;
                case NetworkEventType.DisconnectEvent:
                    if (recHostId == clientSocketID)
                    {
                        Debug.Log("Client: got disconnected");
                        isClientConnected = false;
                    }
                    break;
            }
        } while (networkEvent != NetworkEventType.Nothing);
    }
}